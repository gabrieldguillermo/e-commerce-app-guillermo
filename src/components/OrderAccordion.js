import {Accordion} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
 // import { faFileLines } from '@fortawesome/free-solid-svg-icons';
 import { faFileExcel} from '@fortawesome/free-regular-svg-icons';
 

export default function OrderAccordion({data}) {

  if(data._id === false){
    return (      
            <>
           <h6  align="center" className="text-secondary ">No Orders Yet! </h6>
            <div className="display-1 mt-4 d-flex justify-content-center text-danger " >
                <FontAwesomeIcon icon={faFileExcel} /> 
            </div>
           </>
      )
  }

  const{purchasedOn, products, _id,totalAmount} = data
  const date = new Date(purchasedOn);  
 
   const product = products.map((val, key) => {

          return (
            <tr key={key}>
              <td>{val.productName}</td>
              <td>{val.price}</td>
              <td>x{val.quantity}</td>
              <td>
                  {val.subtotal.toLocaleString('en-US', {
                    style: 'currency',
                    currency: 'PHP',
                   })}
              </td>
             
            </tr>
          )
        })

  return (

        
          
          <Accordion className="purchase-accordion text-secondary">
            <Accordion.Item eventKey="0">
              <Accordion.Header> 
                  <div className="row  w-100  text-secondary">
                   <span className="col  ">{`Purchase ID : ${_id}`}</span>
                   <span className="col text-end">{`${date.toDateString()} / ${date.toLocaleTimeString()}`}</span>
                   </div> 
                </Accordion.Header>
              <Accordion.Body className="">
                   <table className="table">
                   <thead className="text-secondary">
                   <tr>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Sub Total</th>
                 
                </tr>
              </thead>
              <tbody className="text-secondary">
                {product}    
              </tbody>
              <tfoot className="text-secondary">
                   <tr>
                  <th></th>
                  <th></th>
                  <th className="text-end">Total Amount</th>
                  <th>
                    {
                      totalAmount.toLocaleString('en-US', {
                          style: 'currency',
                          currency: 'PHP',
                      })
                    }
                  </th>
                 
                </tr>
              </tfoot>
            
            </table>
              </Accordion.Body>
            </Accordion.Item>
            
          </Accordion>
  
           
          
   
  );
}