
import OrderAccordion from './OrderAccordion';

export default function Posts({posts,loading}){
	return (
		<ul className="list-group">
		{ posts.map( post => {
		return	<li key={post._id} className="list-group-item">
				 <OrderAccordion key={post._id} data={post}/>
			</li>
		})}	


	</ul>
	)
}