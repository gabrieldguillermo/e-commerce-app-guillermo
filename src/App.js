import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';



// components
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer'

//pages
//admin
import Dashboard from './pages/AdminDashboard';
import Products from './pages/Products';
import AddProduct from './pages/AddProduct';
import Users from './pages/Users';
import AllOrders from './pages/AllOrders';

//user
import Home from './pages/Home';
import ViewProducts from './pages/ViewProducts';
import Order from './pages/Order';
import Cart from './pages/Cart';
import Account from './pages/Account';

import ProductbyId from './pages/ProductById';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Error from './pages/Error';
import './App.css';

import { UserProvider } from './UserContext';

function App() {

    const [user, setUser] = useState({
        id:null,
        isAdmin:null
    });

       const unsetUser = () => {
        
        localStorage.removeItem('token');
    }

    useEffect(()=>{

        fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data._id !== "undefined"){
                setUser({
                    id: data._id,
                    isAdmin:data.isAdmin
                })

            } else {

                setUser({
                    id:null,
                    isAdmin:null
                })
            }
        })
        
        
    },[]);
 

  return (
 <UserProvider value={{user, setUser, unsetUser}}>
   <Router>  
       <AppNavbar/>
       <Container className=" mt-5 ">
           <Routes>
               <Route path="/dashboard" element={<Dashboard/>} />
               <Route path="/products" element={<Products/>} />
               <Route path="/products/addProduct" element={<AddProduct/>} />
               <Route path="/users" element={<Users/>} />
               <Route path="/allorder" element={<AllOrders/>} />

               <Route path="/" element={<Home/>} />
               <Route path="/viewproducts" element={<ViewProducts/>} />
               <Route path="/order" element={<Order/>} />
               <Route path="/account" element={<Account/>} />               
               <Route path="/cart" element={<Cart/>} />               
               <Route path="/product/:productId" element={<ProductbyId/>} />
               <Route path="/login" element={<Login/>} />
               <Route path="/logout" element={<Logout />} />
               <Route path="/register" element={<Register/>} /> 
               <Route path="*" element={<Error />} />
           </Routes>
       </Container> 
       <Footer/>      
   </Router>
</UserProvider>
  );
}

export default App;
