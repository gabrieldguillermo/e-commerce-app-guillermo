import {useState, useEffect ,useContext} from 'react';
import { Navigate } from 'react-router-dom';
import {Row } from 'react-bootstrap'; 


//components
import UserContext from '../UserContext';
import ProductCards from '../components/ProductCards';


export default function Products(){
	
const {user} = useContext(UserContext);

const [products,setProducts] = useState([]);



	const[search, setSearch]= useState("");
	const [filteredData,setFilteredData]= useState([]);
	const [message, setMessage] = useState("");

//get all products that are active
const productData= async() => {
  try{
    const product = await fetch(`${process.env.REACT_APP_API_URL}/products/viewProducts`);
    const data = await product.json();

    setProducts(data);
    setFilteredData(data);
 
  } catch (error) {
    console.log(error);
  }
};

useEffect(()=> {
  productData();

},[]);

useEffect(()=>{
	  const result = products.filter(product => {
	   
	      return product.productName.match(search) || product.description.match(search) 

	  });
	 setFilteredData(result);
	

	},[search])

useEffect(()=>{
	 if(filteredData.length == 0){
	 	setMessage('No Data Available!')
	 }else{
	 	setMessage('')
	 }
},[filteredData])
	


const productsCards= filteredData.map((product) => {

	return(
		<ProductCards key={product._id} props={product}/>
		)
});

	return (
		
		(user.id !== null && user.isAdmin) ? 
			<Navigate to="/dashboard" />
		:
		<>
		<input 
			className="form-control ms-auto mb-4"
			type="text" 
			value={search}
			style={{width:"350px"}}
			onChange={e => setSearch(e.target.value) }
			placeholder="Search . . ."
			/>
		<Row className="productCards px-sm-0 px-3">
			
			{productsCards}
			<div className="d-flex justify-content-center my-5">
				<h3 className="text-secondary" >{message}</h3>
			</div>
			
		</Row>

</>
	)
}