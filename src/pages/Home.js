import { useState,useEffect, useContext} from 'react';
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext';

//component
import Banner from '../components/Banner';


	
export default function Home(){
	
const {value} = useContext(UserContext);
	const data = {
		title: "E-commerce-app-guillermo",
		content: "One Stop online Depot.",
		destination: "/viewProducts",
		label: "See Products"
	}


	return (
		
			<Banner data={data}/>
	
	)
};