import {useState, useEffect, useContext } from 'react';
import { Form, Button,Col ,Row,NavLink} from 'react-bootstrap';
import { Link, Navigate ,useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



export default function Login() {

	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

 
async function  loginUser(e){
	e.preventDefault();
	try {
		const login = await fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method:"POST",
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email:email,
				password: password
			})
		});
	
	
		const data = await login.json();
		
		if (data.access && data.access !== "undefined"){
			localStorage.setItem('token',data.access);
			 retrieveUserDetails(data.access);
				Swal.fire({
				title:"Login Succesfull",
				icon: "success",
				text:"You have Logged in Succesfully!"
			});

			setEmail("");
			setPassword("");
		}  else {
			Swal.fire({
				title:"Authentication Failed",
				icon: "error",
				text:"Please, check your login details and try again!"
			});
			return;
		}

	} catch (error){
		console.error(error)
	}		
};


 const retrieveUserDetails = (token) => {

 	fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
 		headers:{
 			Authorization: `Bearer ${ token }`
 		}
 	})
 	.then( res => res.json())
 	.then(data => {
 		// console.log(data);
 		setUser({
 			id: data._id,
 			isAdmin: data.isAdmin
 		})

 	})
 	.catch(error => {
 		console.error(error);
 	})
 		 	
 };

 	useEffect(() => {
 		if(email !== "" && password !== "" ) {
 			setIsActive(true)
 		}else{
 			setIsActive(false)
 		}
 	},[email,password]);
    return (

    	(user.id !== null && user.isAdmin) ?
    		<Navigate to="/dashboard" />
    	:(user.id !== null && !user.isAdmin) ?
    		<Navigate to="/" />
    	:
    	<Row className="justify-content-center"> 
    		<Col className= "col-10  col-md-8 col-lg-6">

    		 <Form className="mt-2 border shadow p-3 bg-white text-secondary ounded" onSubmit= {(e)=> loginUser(e)}>
    		 	<h3 align="center">Login</h3>
	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        	type="email" 
	        	value ={email}
	        	onChange={(e)=> setEmail(e.target.value.replace(/\s/g, ""))} 
	        	placeholder="Enter Your Email"
	        	required
	        	/>
	      </Form.Group>

	      <Form.Group className="mb-5" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	value={password}
	        	onChange={(e)=> setPassword(e.target.value.replace(/\s/g, "")) }
	        	placeholder="Enter Your Password"
	        	required 
	        	autoComplete="off"
	        	 />
	        	
	      </Form.Group>
	     	{ isActive ?
	     		 <Button variant="success" type="submit" id="submitBtn" className="w-100 mb-3">
	       		 Submit
	     		 </Button>
	     		 :
	     		 <Button variant="success" type="submit" id="submitBtn"  className="w-100 mb-3" disabled>
	       		 Submit
	     		 </Button>
	   	  	}
	   	  	<hr />
	   	  	<p className="text-center">Don't have an account yet?
	   	  	<button 
	   	  		type="button"  
	   	  		className="btn btn-link  btn-sm " 
	   	  		onClick={()=> navigate("/register") } 
	   	  		>
	   	  		Click here</button>to register
	   	  	</p>
			
	    	</Form>

    		</Col>
    	</Row>	
    	
       

    );
}