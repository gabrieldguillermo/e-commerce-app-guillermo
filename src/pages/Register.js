import {useState, useEffect, useContext } from 'react';
import { Form, Button,Row,Col } from 'react-bootstrap';
import { Navigate , useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

const {user} = useContext(UserContext);



const navigate = useNavigate();
	
const [firstName, setfirstName] = useState('');
const [lastName, setlastName] = useState('');
const [email, setEmail] = useState('');
const [address, setAddress] = useState('');
const [mobileNo, setMobileNo] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');
const [isActive, setIsActive] = useState(false);


async function registration(e) {
	e.preventDefault();
	try {

		const duplicateEmail = await checkEmail();

		if(duplicateEmail === true){
			Swal.fire({
	            title: "Duplicate Email Found",
	            icon: "error",
	            text: "The email you are registering already exist. Please use a different one",
	        });	

			return;

		} else {
			
		const registerUser = await fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
			method:"POST",
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				firstName:firstName,
				lastName: lastName,
				address: address,
				email:email,
				mobileNo:mobileNo,
				password: password1
			})
		});		
				
		const res = await registerUser.json();
		// console.log(res);
			if(res === true){
				Swal.fire({
					title:"Registration Succesfull",
					icon: "success",
					text:"Your registration is Succes!"
				});

				setfirstName("");
				setlastName("");
				setEmail("");
				setAddress("");
				setMobileNo("");
				setPassword1("");
				setPassword2("");

				navigate("/login");

			} else {
				Swal.fire({
					title:"Something went wrong!",
					icon: "error",
					text:"Please, check your Registration details and try again!"
 				});	
			}	
		}

	} catch (error) {
		console.log(error);
	}	
};		



//check email if exist
const checkEmail = async () => {
try{
	
	const userEmail = await fetch(`${process.env.REACT_APP_API_URL}/users/verifyEmail`,{
			method:"POST",
			headers:{
				'Content-type':'application/json'
			},
			body:JSON.stringify({
				email:email
			})
		});

	const data = await userEmail.json();
	return data

	} catch (error){
		console.log(error);
	}
};


useEffect(() => {
	if((firstName!== "" &&
		lastName.trim() !== "" &&
		mobileNo.length === 11 &&
		email !== "" &&
		address !== "" &&
		password1 !== "" &&
		password2 !== "" 
		) && (password1 === password2)){
		setIsActive(true)
	}else{
		setIsActive(false)
	}
},[firstName,lastName,email,address,mobileNo,password1,password2]);


    return (

    	(user.id !== null ) ?
    		<Navigate to="/" />
    	:
    	<Row className="justify-content-center mb-5">
    		<Col className="col-10  col-md-8 col-lg-6">

        <Form className="border shadow p-3 bg-white rounded text-secondary" onSubmit= {(e)=> registration(e)}>

        		<h3 align="center" >Register</h3>
        <Form.Group className="mb-3 " controlId="firstName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control 
	        	type="text" 
	        	value ={firstName}
	        	onChange={(e)=> setfirstName(e.target.value.trim() )}
	        	placeholder="Enter Your First Name"
	        	required
	        	/>
	    
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="lastName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control 
	        	type="Text" 
	        	value ={lastName}
	        	onChange={(e)=> setlastName(e.target.value.trim() )} 
	        	placeholder="Enter Your Last Name"

	        	required
	        	/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        	type="email" 
	        	value ={email}
	        	onChange={(e)=> setEmail(e.target.value.replace(/\s/g, ""))} 
	        	placeholder="Enter Your Email"
	        	autoComplete="off"
	        	required
	        	/>
	      </Form.Group>

	        <Form.Group className="mb-3" controlId="address">
	        <Form.Label>Address</Form.Label>
	        <Form.Control 
	        	type="Text" 
	        	value ={address}
	        	onChange={(e)=> setAddress(e.target.value)} 
	        	placeholder="Enter Your Address"
	        	required
	        	/>
	      </Form.Group>


	      <Form.Group className="mb-3" controlId="mobileNo">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control 
	        	type="Text" 
	        	value ={mobileNo}
	        	onChange={(e)=> setMobileNo(e.target.value.replace(/\s/g, ""))} 
	        	placeholder="09876543211"
	        	required
	        	/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	value={password1}
	        	onChange={(e)=> setPassword1(e.target.value.replace(/\s/g, "")) }
	        	placeholder="Enter Your Password"
	        	 autoComplete="new-password"
	        	required
	        	 />
	      </Form.Group>
	  		
	      <Form.Group className="mb-3" controlId="password2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control  
	        type="password" 
	        value={password2}
	        onChange={(e)=> setPassword2(e.target.value) }
	        placeholder="Verify Your Password"
	        autoComplete="new-password" 
	        required
	        />
	      </Form.Group>
	     	{ isActive ?
	     		 <Button variant="primary" type="submit" id="submitBtn">
	       		 Submit
	     		 </Button>
	     		 :
	     		 <Button variant="primary" type="submit" id="submitBtn" disabled>
	       		 Submit
	     		 </Button>
	     	}
	     		<hr />
	   	  	<p className="text-center">Already have an account?
	   	  	<button 
	   	  		type="button"  
	   	  		className="btn btn-link  btn-sm " 
	   	  		onClick={()=> navigate("/login") } 
	   	  		>
	   	  		Click here</button>to login
	   	  	</p>
	    </Form>
    		</Col>
    	</Row>	

    );
}