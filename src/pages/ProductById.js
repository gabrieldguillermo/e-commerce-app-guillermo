
import {useState, useEffect, useContext} from 'react';
import { useParams ,useNavigate, Link} from 'react-router-dom';
import {Row, Col ,Button, Card } from 'react-bootstrap'; 

import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductById(){
const cartFromLocalStorage = JSON.parse(localStorage.getItem('Cart') || '[]');
	const {user} = useContext(UserContext);
	const { productId } = useParams();
	const navigate = useNavigate();

	const[cart, setCart] = useState(cartFromLocalStorage);


	const [productName, setProductName]= useState("");
	const [description, setDescription]= useState("");
	const [price ,setPrice]= useState("");
	const [image, setImage]= useState("");
	const [quantity, setQuantity]= useState(1);
	const [instock, setInstock]= useState("");
	// const [total, setTotal] =useState("");



	const productDetails=()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price)
			setImage(data.imageUrl);
			setInstock(data.quantity);
		})
		.catch(error => {
        console.log(error);
      	});

		
	}

// confirm order
	const confirm =(e) =>{
		e.preventDefault();
		Swal.fire({
		  title: 'Are you sure?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Place an order!'
		}).then((result) => {
		  if (result.isConfirmed) {
		    	order();
		  }
		})
	};

	//Post an order to the database
	const order = ()=>{	 
    fetch(`${process.env.REACT_APP_API_URL}/orders`, {
        method: "Post",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
	      },
	      body: JSON.stringify({
          products: [
          	{
          		productId: productId,
          		productName: productName,
          		imageUrl: image,
          		price: price,
          		quantity: quantity,
          		subtotal: price * quantity  
        		}
          ]
	      })
    })
    .then(res => res.json())
    .then( data => {
    	if(data === true){
			Swal.fire({
		  title: 'Order has been placed',
		  text: 'Thank you for ordering our products!',
		  icon: 'success',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'See my purchase'
		}).then((result) => {
		  if (result.isConfirmed) {
		    	navigate("/order")
		  }else{
		  	productDetails();	
		  }
		})
			

    	}else{
    		Swal.fire({
			   title:"Something Went Wrong!",
			   icon: "Error"
			});
    	}
    })
}



	const getQuantity=(num)=>{
		const number0fQuantity= quantity + num;
		if(number0fQuantity){
		setQuantity(quantity + num)
		}else{
			return;
			// alert('quantity cannot be a Zero')
		}
	}

	useEffect(() => {
		productDetails();	
	},[productId])

	useEffect(()=>{
	localStorage.setItem('Cart',JSON.stringify(cart))
	
	},[cart])

	
	const addToCart=()=>{
	 const product ={
	 	userId:user.id,
	 	productName:productName,
	 	productId:productId, 
	 	quantity: quantity,
	 	price: price,
	 	imageUrl: image,
	 	
	 }

		setCart([...cart,product]);
		
	}

	const total= (quantity * price).toLocaleString('en-US', {
			  style: 'currency',
			  currency: 'PHP',
			});

	const stringTotal= (total).toLocaleString('en-US', {
	  style: 'currency',
	  currency: 'PHP',
	})

	const stringPrice=(price).toLocaleString('en-US', {
	  style: 'currency',
	  currency: 'PHP',
	})


	return (
		<Row className="justify-content-center ">
			<Col className=" col-sm-10 col-md-8 col-lg-6 mb-5"   >
		   		<form  onSubmit={(e) => confirm(e)}>  
					<Card className="shadow">
					  	<div className="p-5 card-img " >
							 <Card.Img  src={image}   />
						</div>

		      		<Card.Body className="p-4">

				       	<div className= "mb-2">
				        <Card.Title className="d-inline me-2">{productName}</Card.Title> 
				        </div>

				        <div className= "mb-2">
				        	<Card.Subtitle className="d-inline  me-2">Description:</Card.Subtitle>
				        	<Card.Text className="d-inline">{description}</Card.Text>
				        </div>

		        		<div className= "mb-2">
					        <Card.Subtitle  className="d-inline  me-2">Price:</Card.Subtitle>
					         <Card.Text  className="d-inline">{stringPrice}</Card.Text>
		        		</div>

		        		<div className= "mb-2">
		         			<Card.Subtitle  className="d-inline  me-2">Products Available:</Card.Subtitle>
		         			<Card.Text  className="d-inline">{instock}</Card.Text>
						</div>

		        		<div className="mb-2 py-2"> 
		        			<Card.Subtitle  className="mb-1">Quantity:</Card.Subtitle>
		        			<div className="btn-group" role="group">
			        			<Button size="text-bold" onClick={()=> getQuantity(-1)}>-</Button>		  
			        			<Button size="btn btn-light  border border-primary" >{quantity}</Button>
			        			<Button size="text-bold" onClick={()=> getQuantity(1)} >+</Button>
		        			</div>
		        		</div>

		        	    <div className= "mb-2">
				         	<Card.Subtitle  className="d-inline  me-2" >Total:</Card.Subtitle>
				         	<Card.Text  className="d-inline"> {stringTotal} </Card.Text>
		        		</div>
		       
		      		</Card.Body>
					    <div className="px-5 mb-3">
					    	{
					      	(user.id !== null) ?
					        <>	
					      		<Button  className="w-100 btn-danger mb-2" 
					      		onClick={()=> addToCart()}
					      		>Add to cart</Button>
					      		<Button className="w-100  mb-2" type="submit" id="submitBtn">Buy now</Button>
					     	</>
					      	:
					        <>	
					      		<Button  className="w-100 btn-danger mb-2" as={Link} to="/login">Add to cart</Button>
					      		<Button className="w-100  mb-2" as={Link} to="/login">Buy now</Button>
					        </>
					    	}
					    </div>
		    		</Card>
		    	</form>
	    	</Col>
		</Row> 
	)
}