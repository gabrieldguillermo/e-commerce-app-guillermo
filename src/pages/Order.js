import	{ useState , useEffect, useContext} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import OrderAccordion from '../components/OrderAccordion'; 

export default function Purchase(){
    const {user} = useContext(UserContext);
 
	const[data, setData] = useState([]);
    
    //get user orders
    const userOrders=async ()=>{

     try{

       const orders= await fetch(`${process.env.REACT_APP_API_URL}/orders`,{
            method: "GET",
            headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const data = await orders.json();
        if(data.length > 0){
            setData(data);  
        }else{
            setData([{_id:false,}])
        }
        
       
     } catch (error) {
        console.log(error)
     }

    };

    useEffect(()=>{
    	userOrders();
       
    },[]);


    const orders=[];
        data.forEach(d => {
            orders.unshift(d);
        });

    const orderDetails = orders.map(order=>{
        return (
            <OrderAccordion key={order._id} data={order}/>
            )
    });

	return (
            
                (user.id !== null && !user.isAdmin) ? 
                <>
                <h3 className="text-secondary">My Orders</h3>                                    
                <div className="accordion-container w-100">
                     {orderDetails}
                </div>                   
                  </>          	   
            : (user.id !== null && user.isAdmin) ? 
            <> <Navigate to="/dashboard" /> </>
            : <Navigate to="/" />
            
            
  );
		
}