import { useState, useEffect, useContext} from 'react';
import { Navigate } from 'react-router-dom'
import DataTable from 'react-data-table-component';


import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function User(){

	const { user } = useContext(UserContext);

	const[search, setSearch]= useState("");
	const [filteredUser,setFilteredUser]= useState([]);
	const [allUsersData, setallUsersData] = useState([]);


	const getAllUser= async() => {
	  try{
	    const allUsers = await fetch(`${process.env.REACT_APP_API_URL}/users/all`,{
	        method: "GET",
	        headers: {
	        "Content-Type": "application/json",
	        "Authorization": `Bearer ${localStorage.getItem('token')}`
	      	}
	    });

	    const usersData = await allUsers.json();
	    // console.log(usersData);
	    setallUsersData(usersData);
	    setFilteredUser(usersData);
	    
	  } catch (error) {
	    console.log(error);
	  }
	};

	//get all user
	useEffect(()=> {
	  getAllUser();

	},[]);


	const confirm=(id,status)=>{
	  Swal.fire({
	  title: 'Are you sure?',
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: "Update"
	}).then((result) => {
	  if (result.isConfirmed) {
	  	updateIsAdmin(id,status); 
	  }else{
	  	return;
	  }
	})
	};

	const updateIsAdmin = (id,status) =>{
		let isAdmin="";
		if (status) {
			isAdmin = false
		}else{
			isAdmin= true
		}

		fetch(`${process.env.REACT_APP_API_URL}/users/details/${id}`,{
			  method: "PATCH",
	   		  headers: {
	    	  "Content-Type": "application/json",
	    	  "Authorization": `Bearer ${localStorage.getItem('token')}`
	    	 },
	    	  body: JSON.stringify({
	          isAdmin:isAdmin 
      		})
		})
		.then( res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
		          title:"Update Success",
		          icon: "success"
		       });
				 getAllUser();
			} else {
				Swal.fire({
		          title:"Somthing Went Wrong",
		          icon: "error"
		       });
			}
		})
	}

	// filter/search allUsersData
	useEffect(()=>{
	  const result = allUsersData.filter(user => {
	    let status = ""
	    if(user.isAdmin === true){
	      status = "Admin"
	    }else{
	    status = "User"
	    }
	      return user.firstName.match(search) || user.lastName.match(search) || user.email.match(search) || user.address.match(search) || user.mobileNo.match(search) || status.match(search); 

	  });
	 setFilteredUser(result);
	},[search])


	const columns = [
	  {
	    name: "Last Name",
	    selector: (row) => row.lastName
	  },
	  {
	    name: "First Name",
	    selector: (row) => row.firstName,
	   

	  },
	  {
	    name: "Email",
	    selector: (row) => row.email,
	    
	  },
	  {
	    name: "Address",
	    selector: (row) => row.address,
	    
	  },
	  {
	    name: "Mobile No.",
	    selector: (row) => row.mobileNo
	  },
	  {
	    name: "IsAdmin",
	    selector: (row) => row.isAdmin ? 'Admin' : 'User'
	   
	  },
	  {
	    name:"Action",
	    width:"160px",
	    cell:(row)=> {
	      return ( 
	      <div className="d-flex flex-wrap " > 
	        {
	         row.isAdmin ? 
	         <button
	        className="btn btn-sm btn-secondary px-1 mt-1" 
	        onClick={e => confirm(row._id, row.isAdmin)}> Set as User</button>
	        :
	         <button
	        className="btn btn-sm btn-primary px-1 mt-1" 
	        style={{width:'100px'}}
	         onClick={e => confirm(row._id,row.isAdmin)}>Set as Admin</button>
	      }
	      </div>
	      )
	    }

	  }
	];


	return (
			(user.id !== null && user.isAdmin) ?
		<>
			<h4 className="text-secondary">Users List</h4>
			 <div className="border shadow"> 
		        <DataTable 
		     	  className="" 
			      columns={columns} 
			      data={filteredUser}
			      pagination 
			      fixedHeader
			      highlightOnHover
			      subHeader
			      subHeaderComponent = {
			        <input 
			          type ="text"
			          className = " mt-4 form-control"
			          size="sm"
			          style={{width:"200px"}}
			          placeholder ="Search . . ."
			          value={search}
			          onChange={e => setSearch(e.target.value) }
			          />
			      }
		      />
		     </div> 
		</>
		: <Navigate to="/" />
		)
}