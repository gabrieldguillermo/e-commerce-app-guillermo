import Banner from '../components/Banner';
import {useContext} from 'react'
import UserContext from '../UserContext';

export default function Error () {

const {user} = useContext(UserContext);
	let route = "";
	if(user.isAdmin === true){
		route="/dashboard"
	}else {
		route= "/"
	}
	const data = {
		title: "404 - Page Not Found",
		content: "The page you are looking cannot be found.",
		destination: route,
		label: "Back to Home"
	}

	return (

		<Banner data={data} />

	)
};
