import {useState} from 'react';
import {Table,Container} from 'react-bootstrap';


export default function Cart(){

	const cartFromLocalStorage = JSON.parse(localStorage.getItem('Cart') || '[]');
	const[cart, setCart] = useState(cartFromLocalStorage);
	

	const productInCart = cart.map( (item, index)=>{
		const sub =item.price * item.quantity

		return(
			<tr key={index}>
				<td >{item.imageUrl}</td>	
				<td >{item.productName}</td>	
				<td >{item.price}</td>	
				<td >{item.quantity}</td>	
				<td >{sub}</td>	
				
			</tr>
			)
	})

	return (
		<Container>
			<Table responsive>
			      <thead>
			        <tr>
			          <th>Image</th>
			          <th>Name</th>
			          <th>Price</th>
			          <th>Quantity</th>
			          <th>Sub Total</th>
			          
			           
			        </tr>
			      </thead>
			      <tbody>
			      {productInCart}
			        
			      </tbody>
			    </Table>
		</Container>
		)
}