import {useContext} from 'react';
import {useNavigate} from 'react-router-dom'
import UserContext from '../UserContext';
import Banner from '../components/Banner'

export default function Dashboard(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const data = {
		title: "E-commerce-app-guillermo",
		content: "One Stop online Depot.",
		destination: "/products",
		label: "See Products"
	}
	if(!user.isAdmin){
		navigate("/")
	}
	return (
		<>
		<h3>Dashboard</h3>
			<Banner data={data}/>
		</>
		)
		

}